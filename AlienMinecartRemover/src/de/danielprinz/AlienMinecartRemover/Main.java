package de.danielprinz.AlienMinecartRemover;

import de.danielprinz.AlienMinecartRemover.Commands.AlienMinecartRemoverCommandExecutor;
import de.danielprinz.AlienMinecartRemover.Listener.PlayerInteractListener;
import de.danielprinz.AlienMinecartRemover.Listener.VehicleDestroyListener;
import de.danielprinz.AlienMinecartRemover.Listener.VehicleEnterListener;
import de.danielprinz.AlienMinecartRemover.Listener.VehicleExitListener;
import de.danielprinz.AlienMinecartRemover.Methods.CartRemover;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.*;

/**
 * Created by daniela on 21.10.2016.
 */
public class Main extends JavaPlugin {

    public FileConfiguration cfg;
    public String prefix;

    public ArrayList<Player> tmpPlayerDeactivation;
    public HashMap<UUID, CartRemover> knownCarts;
    public ArrayList<UUID> killOnChunkLoad;

    public boolean enabled;
    public int interval;
    public String text_minecart_place;
    public String text_minecart_receive;

    @Override
    public void onEnable() {
        this.loadPlugin();
    }

    @Override
    public void onDisable() {
        this.unloadPlugin();
    }

    public void restartPlugin() {
        this.unloadPlugin();
        this.reloadConfig();
        this.loadPlugin();
    }

    public void loadPlugin() {
        this.cfg = this.getConfig();
        this.prefix = ChatColor.GRAY + "[" + ChatColor.DARK_RED + "AMR" + ChatColor.GRAY + "] " + ChatColor.AQUA;

        this.tmpPlayerDeactivation = new ArrayList<Player>();
        this.knownCarts = new HashMap<UUID, CartRemover>();
        this.killOnChunkLoad = new ArrayList<UUID>();

        this.enabled = false;
        this.interval = 3*60*20;
        this.text_minecart_place = "";
        this.text_minecart_receive = "";

        new AlienMinecartRemoverCommandExecutor(this, "alienminecartremover", "Removes empty minecarts in a given interval", "amr");

        if(!this.cfg.contains("configuration")) {
            this.error("noConfig");
            return;
        }

        Set<String> configuration = this.cfg.getConfigurationSection("configuration").getKeys(true);
        if(!configuration.contains("enabled") || !configuration.contains("interval") || !configuration.contains("text_minecart_place") || !configuration.contains("text_minecart_receive")) {
            this.error("configError");
            return;
        }

        this.enabled = this.cfg.getBoolean("configuration.enabled");
        this.text_minecart_place = this.cfg.getString("configuration.text_minecart_place");
        this.text_minecart_place = ChatColor.translateAlternateColorCodes('&', this.text_minecart_place);
        this.text_minecart_receive = this.cfg.getString("configuration.text_minecart_receive");
        this.text_minecart_receive = ChatColor.translateAlternateColorCodes('&', this.text_minecart_receive);
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("js");
            this.interval = Integer.parseInt(String.valueOf(engine.eval(this.cfg.getString("configuration.interval"))));
        } catch (ScriptException e) {
            this.error("configError");
            return;
        }

        // register the listeners for minecarts being placed, destroyed, ...
        Listener playerInteractListener = new PlayerInteractListener(this);
        Bukkit.getPluginManager().registerEvents(playerInteractListener, this);

        Listener vehicleEnterEvent = new VehicleEnterListener(this);
        Bukkit.getPluginManager().registerEvents(vehicleEnterEvent, this);

        Listener vehicleExitListener = new VehicleExitListener(this);
        Bukkit.getPluginManager().registerEvents(vehicleExitListener, this);

        Listener vehicleDestroyEvent = new VehicleDestroyListener(this);
        Bukkit.getPluginManager().registerEvents(vehicleDestroyEvent, this);

    }

    public void unloadPlugin() {
        this.enabled = false;
        for(Map.Entry<UUID, CartRemover> cart : knownCarts.entrySet()) {
            cart.getValue().stopTask();
            cart.getValue().removeCart();
        }
    }

    public void error(String error, String... args) {
        if(error.equals("noConfig")) {
            this.cfg.set("configuration", new HashMap<String, Object>(){{
                put("enabled", true);
                put("text_minecart_place", "Dein Minecart wird nach 3 Minuten entfernt, sollte es leer stehen");
                put("text_minecart_receive", "Du erhältst dein Minecart zurück, weil es nicht mehr benutzt wurde");
                put("interval", "3*60*20");
            }});
            this.saveConfig();

            this.getLogger().severe("=============================================================================");
            this.getLogger().severe("== You should review the config in plugins/AlienMinecartRemover/config.yml ==");
            this.getLogger().severe("=============================================================================");
            return;
        }

        if(error.equals("configError")) {
            this.getLogger().severe("=============================================================================");
            this.getLogger().severe("== Error in configuration file in plugins/AlienMinecartRemover/config.yml. ==");
            this.getLogger().severe("============== Remove to generate a clean configuration file.  ==============");
            this.getLogger().severe("========================== Plugin was NOT loaded.  ==========================");
            this.getLogger().severe("=============================================================================");
            return;
        }
    }




}
