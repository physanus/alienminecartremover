package de.danielprinz.AlienMinecartRemover.Listener;

import de.danielprinz.AlienMinecartRemover.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleDestroyEvent;

/**
 * Created by daniela on 28.10.2016.
 */
public class VehicleDestroyListener implements Listener {

    private Main plugin;
    public VehicleDestroyListener(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onVehicleDestroy(VehicleDestroyEvent e) {

        if (!this.plugin.enabled)
            return;

        if(this.plugin.knownCarts.containsKey(e.getVehicle().getUniqueId())) {
            this.plugin.knownCarts.get(e.getVehicle().getUniqueId()).stopTask();
            this.plugin.knownCarts.remove(e.getVehicle().getUniqueId());
        }


    }

}
