package de.danielprinz.AlienMinecartRemover.Listener;

import de.danielprinz.AlienMinecartRemover.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleExitEvent;

/**
 * Created by daniela on 28.10.2016.
 */
public class VehicleExitListener implements Listener {

    private Main plugin;
    public VehicleExitListener(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onVehicleExit(VehicleExitEvent e) {

        if (!this.plugin.enabled)
            return;

        if(this.plugin.knownCarts.containsKey(e.getVehicle().getUniqueId()))
            this.plugin.knownCarts.get(e.getVehicle().getUniqueId()).startTask();

    }

}
