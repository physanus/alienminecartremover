package de.danielprinz.AlienMinecartRemover.Listener;

import de.danielprinz.AlienMinecartRemover.Main;
import de.danielprinz.AlienMinecartRemover.Methods.CartRemover;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by daniela on 28.10.2016.
 */
public class PlayerInteractListener implements Listener {

    private Main plugin;
    public PlayerInteractListener(Main plugin) {
        this.plugin = plugin;
    }


    @EventHandler
    public void onBlockPlace(PlayerInteractEvent e) {

        if (!this.plugin.enabled)
            return;

        final Player p = e.getPlayer();

        if(e.getItem() == null)
            return;

        if(!e.getItem().getType().equals(Material.MINECART))
            return;
        if(!e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
            return;

        if(!e.getClickedBlock().getType().equals(Material.RAILS) &&
           !e.getClickedBlock().getType().equals(Material.ACTIVATOR_RAIL) &&
           !e.getClickedBlock().getType().equals(Material.DETECTOR_RAIL) &&
           !e.getClickedBlock().getType().equals(Material.POWERED_RAIL))
            return;


        if(this.plugin.tmpPlayerDeactivation.contains(p))
            return;
        this.plugin.tmpPlayerDeactivation.add(p);
        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, new Runnable() {
            @Override
            public void run() {
                plugin.tmpPlayerDeactivation.remove(p); // Event is triggered twice; this is a workaround
            }
        });


        p.sendMessage(this.plugin.prefix + ChatColor.DARK_AQUA + this.plugin.text_minecart_place);
        new CartRemover(this.plugin, p).init();


    }

}
