package de.danielprinz.AlienMinecartRemover.Listener;

import de.danielprinz.AlienMinecartRemover.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleEnterEvent;

/**
 * Created by daniela on 28.10.2016.
 */
public class VehicleEnterListener implements Listener {

    private Main plugin;
    public VehicleEnterListener(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onVehicleEnter(VehicleEnterEvent e) {

        if (!this.plugin.enabled)
            return;

        if(this.plugin.knownCarts.containsKey(e.getVehicle().getUniqueId()))
            this.plugin.knownCarts.get(e.getVehicle().getUniqueId()).stopTask();

    }

}
