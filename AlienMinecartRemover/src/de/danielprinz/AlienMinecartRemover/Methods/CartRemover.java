package de.danielprinz.AlienMinecartRemover.Methods;

import de.danielprinz.AlienMinecartRemover.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * Created by daniela on 21.10.2016.
 */
public class CartRemover {

    private Main plugin;
    private Minecart minecart;
    private UUID uuid;

    public CartRemover(Main plugin, Player p) {
        this.plugin = plugin;
        this.uuid = p.getUniqueId();
    }

    private int scheduler_id;
    private DecimalFormat df = new DecimalFormat("#.###");

    public void init() {

        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, new Runnable() {
            @Override
            public void run() {

                for (Entity en : Bukkit.getPlayer(uuid).getLocation().getWorld().getEntities()) {
                    if (en.getType().equals(EntityType.MINECART)) {
                        if (!plugin.knownCarts.containsKey(en.getUniqueId())) {
                            // gets just created minecart
                            // we cannot get the minecart from PlayerInteractEvent
                            plugin.knownCarts.put(en.getUniqueId(), getInstance());
                            minecart = (Minecart) en;

                            startTask();
                            break;
                        }
                    }
                }

            }
        });

    }

    public void stopTask() {
        Bukkit.getScheduler().cancelTask(this.scheduler_id);
    }
    public void startTask() {

        this.stopTask(); // cancel task if one is running
        this.scheduler_id = Bukkit.getScheduler().scheduleSyncRepeatingTask(this.plugin, new Runnable() {
            @Override
            public void run() {

                try {
                    Bukkit.getPlayer(uuid).isOnline();
                } catch (NullPointerException e) {
                    // if player is NOT online, NPE will be thrown
                    return;
                }

                try {
                    minecart.getPassenger().getClass();
                    return;
                    // passenger inside
                } catch (NullPointerException e1) {
                    // no passenger inside

                    boolean inWorld = false;
                    for(Entity e : minecart.getLocation().getWorld().getEntities()) {
                        if (e.getUniqueId().equals(minecart.getUniqueId())) {
                            inWorld = true;
                            minecart = (Minecart) e;
                        }
                    }
                    if(!inWorld)
                        return;


                    removeCart();

                    Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {
                        @Override
                        public void run() {
                            boolean successful = true;
                            for(Entity e : minecart.getLocation().getWorld().getEntities()) {
                                if(e.getUniqueId().equals(minecart.getUniqueId())) {
                                    successful = false;
                                }
                            }

                            if(successful) {

                                plugin.getLogger().info("Removed " + Bukkit.getPlayer(uuid).getName() + "'s minecart at x=" + df.format(minecart.getLocation().getX()) + " y=" + df.format(minecart.getLocation().getY()) + " z=" + df.format(minecart.getLocation().getZ()));
                                Bukkit.getPlayer(uuid).sendMessage(plugin.prefix + ChatColor.DARK_AQUA + plugin.text_minecart_receive);
                                plugin.knownCarts.remove(minecart.getUniqueId());
                                stopTask();

                                addItemsToInv(Bukkit.getPlayer(uuid), new ArrayList<ItemStack>() {{
                                    add(new ItemStack(Material.MINECART));
                                }});
                            }
                        }
                    }, 1L);


                }

            }
        }, this.plugin.interval, this.plugin.interval);
    }

    public void removeCart() {
        this.minecart.remove();
    }


    private void addItemsToInv(final Player p, Collection<ItemStack> itemStacks) {
        for(final ItemStack is : itemStacks) {
            if(is == null)
                continue;

            if(!(p.getInventory().firstEmpty() < 0)) {
                // Enough space in inventory
                p.getInventory().addItem(is);
            } else {
                // Drop item above the player
                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        p.getWorld().dropItem(p.getLocation().add(0, 1.5, 0), is);
                    }
                });
            }
        }
    }

    private CartRemover getInstance() {
        return this;
    }

}
