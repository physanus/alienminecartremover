package de.danielprinz.AlienMinecartRemover.Commands;

import de.danielprinz.AlienMinecartRemover.Main;
import de.danielprinz.AlienMinecartRemover.Methods.CommandInitializer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniela on 12.10.2016.
 */
public class AlienMinecartRemoverCommandExecutor extends CommandInitializer<Main> {

    public AlienMinecartRemoverCommandExecutor(Main plugin, String command, String description, String... aliases) {
        super(plugin, command, description, aliases);
    }

    DecimalFormat df = new DecimalFormat("#.###");

    @Override
    public boolean execute(CommandSender cs, String label, String[] args) {

        if(args.length == 1) {
            if (args[0].equals("reload")) {
                if(!cs.hasPermission("alienminecartremover.reload")) {
                    cs.sendMessage(this.plugin.prefix + ChatColor.DARK_RED + "You have no permission to execute this command!");
                    return false;
                }
                this.plugin.restartPlugin();
                cs.sendMessage(this.plugin.prefix + ChatColor.GREEN + "The plugin reloaded successfully.");
                return false;
            }

            if (args[0].equals("clear")) {
                if(!cs.hasPermission("alienminecartremover.clear")) {
                    cs.sendMessage(this.plugin.prefix + ChatColor.DARK_RED + "You have no permission to execute this command!");
                    return false;
                }

                int i = this.removeCarts();

                if(i == 0) {
                    cs.sendMessage(this.plugin.prefix + ChatColor.DARK_AQUA + "Es wurden " + i + " Minecarts entfernt");
                }
                return false;
            }

            cs.sendMessage(this.getHelp());
            return false;
        }

        cs.sendMessage(this.getHelp());
        return false;
    }

    private String getHelp() {

        String s = "";

        s += ChatColor.DARK_AQUA + "\n===== Help for AlienMinecartRemover =====\n" +
                ChatColor.GOLD + "/amr help" + ChatColor.GRAY + " | " + ChatColor.DARK_AQUA + "Shows this page.\n" +
                ChatColor.GOLD + "/amr clear" + ChatColor.GRAY + " | " + ChatColor.DARK_AQUA + "Manually removes empty minecarts.\n" +
                ChatColor.GOLD + "/amr reload" + ChatColor.GRAY + " | " + ChatColor.DARK_AQUA + "Reloads the plugin.";

        return s;

    }

    @Override
    public List<String> tabComplete(CommandSender cs, String label, String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("reload");
        list.add("help");
        list.add("clear");
        return list;
    }


    public int removeCarts() {

        int i = 0;

        for(World world : Bukkit.getWorlds()) {
            for(Entity e : world.getEntities()) {
                i += this.removeCart(e);
            }
        }

        if(i == 0)
            return i;

        String info = "Removed " + i + " minecart";
        info += i == 1 ? "" : "s";

        this.plugin.getLogger().info(info + " in total");
        for(Player all : Bukkit.getOnlinePlayers()) {
            if(all.hasPermission("alienminecartremover.view")) {
                all.sendMessage(this.plugin.prefix + ChatColor.DARK_AQUA + info);
            }
        }

        return i;

    }

    private int removeCart(Entity e) {

        if(e.getType().equals(EntityType.MINECART)) {
            try {
                e.getPassenger().getClass();
                // passenger inside
            } catch (NullPointerException e1) {
                // no passenger inside
                this.plugin.getLogger().info("Removed minecart at x=" + this.df.format(e.getLocation().getX()) + " y=" + this.df.format(e.getLocation().getY()) + " z=" + this.df.format(e.getLocation().getZ()));
                if(this.plugin.knownCarts.containsKey(e.getUniqueId())) {
                    this.plugin.knownCarts.get(e.getUniqueId()).stopTask();
                    this.plugin.knownCarts.remove(e.getUniqueId());
                }
                e.remove();
                return 1;
            }
        }

        return 0;
    }

}
